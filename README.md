## Learning GoLang

[![codecov](https://codecov.io/bb/zaidsa3sa3/go-learn/branch/AccountService/graph/badge.svg?token=FWYJpN615s)](https://codecov.io/bb/zaidsa3sa3/go-learn)


###  Note 

This is a project for learning Go lang.

#### Phase 1 

1. [x] Basics of GRPC
2. [x] How to structure your project
3. [x] How to install vendors
4. [x] How to connect to db using dbr vendor
5. [x] Restructuring the code and the folders related.
6. [x] Refactor the code to use language specific features
7. [x] Learn how to write mocks
8. [x] How to parse config with Environment Variables

#### Phase 2 ####

1. [x] Proto Buffer ver.3
2. [x] GRPC
3. [x] Config, read from evironment variables
4. [x] ErrorCollection
5. [x] Validators Password,Email
6. [x] SyncCommandBus
7. [ ] BaseRepository "EntityManager"
8. [ ] Repository + Entities + EntityMapping
9. [ ] EventManager
10. [ ] Logger
11. [ ] DataTransferableObject DTO + "ReadModel"


#### Phase 2 ####
1. [ ] GRPC with ErrorCollection support
2. [ ] ProcessManager "Saga"
3. [ ] AsyncCommandBus
4. [ ] Refactor Validators to support ErrorCollection
5. [ ] EventSourcing

