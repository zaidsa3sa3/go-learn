package command

import (
	"bitbucket.org/zaidsa3sa3/go-learn/common/Validator"
	"bitbucket.org/zaidsa3sa3/go-learn/common"
)

type RegisterAccount struct{
	Name	string
	Email	string
	Password string
	ConfirmPassword string
	ErrorCollection *common.ErrorCollection
}

func (a *RegisterAccount) IsValid()  bool {
	if Validator.Email(a.Email) == false {
		a.ErrorCollection.Add(
			"email", Validator.InvalidEmail,
		)
	}
	if Validator.Password(a.Password) == false {
		a.ErrorCollection.Add(
			"password", Validator.InvalidPassword,
		)
	}

	return !a.ErrorCollection.HasError()
}


func CreateRegisterAccount(name string, email string, password string, confirmPassword string)  *RegisterAccount {

	return &RegisterAccount{
		Name: name,
		Email: email,
		Password: password,
		ConfirmPassword: confirmPassword,
		ErrorCollection: common.NewErrorCollection(nil),
	}
}