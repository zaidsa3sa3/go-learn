package command

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestCreateRegisterAccount(t *testing.T) {
	assert := assert.New(t)
	expectedName := "zaid"
	expectedEmail := "zaidsa3sa3@gmail.com"
	expectedPassword := "1qaz2wsx"
	expectedConfirmPassword := "1qaz2wsx"
	
	r := CreateRegisterAccount(expectedName,expectedEmail,expectedPassword,expectedConfirmPassword)
	assert.Equal(expectedName,r.Name)
	assert.Equal(expectedEmail,r.Email)
	assert.Equal(expectedPassword,r.Password)
	assert.Equal(expectedConfirmPassword,r.ConfirmPassword)
}

func TestRegisterAccount_IsValid(t *testing.T) {
	assert := assert.New(t)
	expectedName := "zaid"
	expectedEmail := "zaidsa3sa3@gmail.com"
	expectedPassword := "1qaz2wsx"
	expectedConfirmPassword := "1qaz2wsx"
	r := CreateRegisterAccount(expectedName,expectedEmail,expectedPassword,expectedConfirmPassword)
	assert.Equal(true,r.IsValid())
}

func TestRegisterAccount_IsValid_NotValidEmail(t *testing.T) {
	assert := assert.New(t)
	expectedName := "zaid"
	expectedEmail := "zaiom"
	expectedPassword := "1qaz2wsx"
	expectedConfirmPassword := "1qaz2wsx"
	r := CreateRegisterAccount(expectedName,expectedEmail,expectedPassword,expectedConfirmPassword)
	assert.Equal(false,r.IsValid())
}

func TestRegisterAccount_IsValid_NotValidPassword(t *testing.T) {
	assert := assert.New(t)
	expectedName := "zaid"
	expectedEmail := "zaidsa3sa3@gmail.com"
	expectedPassword := "1qaz"
	expectedConfirmPassword := "1qaz"
	r := CreateRegisterAccount(expectedName,expectedEmail,expectedPassword,expectedConfirmPassword)
	assert.Equal(false,r.IsValid())
}