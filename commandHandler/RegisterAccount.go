package commandHandler

import (
	"bitbucket.org/zaidsa3sa3/go-learn/common"
	"bitbucket.org/zaidsa3sa3/go-learn/command"
)

type RegisterAccount struct {
}

func (h *RegisterAccount) Handle(v common.CommandInterface) {
	c := v.(command.RegisterAccount)
	println("print anything just for making sure, because we don't return anything. Name: " + c.Name)


	// TODO: use github.com/goglue/eventmanager so we can start firing events
	// TODO: implement entityManager so we can call DB using DBR vendor
	// TODO: implement Logger so we can start logging
}