package common

type CommandInterface interface {
}

type CommandHandlerInterface interface {
	Handle(v CommandInterface)
}

type CommandBusInterface interface {
	Send(c CommandInterface)
	Register(c CommandInterface ,h CommandHandlerInterface)
}