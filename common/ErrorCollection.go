package common

type Error struct {
	Message	string
}

func (e *Error)Error()  string {
	return  e.Message
}

type ErrorMap map[string]*Error

type ErrorCollection struct {
	errors	ErrorMap
}

func (ec *ErrorCollection) HasError() bool {
	if len(ec.errors) == 0 {
		return false
	}

	return true
}

func (ec *ErrorCollection) Add(s string,m string) {
	ec.errors[s] = &Error{Message:m}
}

func NewErrorCollection(errors ErrorMap) *ErrorCollection {
	if(errors == nil){
		errors = make(map[string]*Error)
	}
	return &ErrorCollection{errors: errors}
}
