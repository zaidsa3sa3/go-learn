package common

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestNewErrorCollection(t *testing.T) {


	err := make(ErrorMap)
	err["password"] = &Error{
		Message: "invalid password",
	}

	assert := assert.New(t)

	ec := NewErrorCollection(err)
	assert.Equal(&ErrorCollection{errors: err}, ec)
	assert.Equal(err,ec.errors)

	ec.Add("koko","xx")

	err["koko"] = &Error{
		Message: "xx",
	}
	assert.Equal(err,ec.errors)
	assert.Equal(true,ec.HasError())


}

func TestNewErrorCollection_HasError_Empty(t *testing.T) {
	assert := assert.New(t)
	ec := NewErrorCollection(nil)
	assert.Equal(false,ec.HasError())
}

func TestError_Error(t *testing.T) {
	assert := assert.New(t)
	expectedMessage := "koko"
	e := &Error{Message:expectedMessage}
	assert.Equal(expectedMessage, e.Error())
}
