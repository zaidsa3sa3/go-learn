package common

import "reflect"

// This is a Synchronous command bus
type MemoryCommandBus struct {
	handlers map[string]CommandHandlerInterface
}

func NewMemoryCommandBus() *MemoryCommandBus {
	return &MemoryCommandBus{handlers: make(map[string]CommandHandlerInterface)}
}

func (b *MemoryCommandBus)Register(c CommandInterface ,h CommandHandlerInterface) {
	b.handlers[reflect.TypeOf(c).Name()] = h
}

func (b *MemoryCommandBus) Send(c CommandInterface) {
	h := b.handlers[reflect.TypeOf(c).Name()]
	h.Handle(c)
}
