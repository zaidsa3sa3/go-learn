package common

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"fmt"
)

type mockCommandHandler struct {
	CommandHandlerInterface
}

func (m *mockCommandHandler) Handle(v CommandInterface) {
	c := v.(CommandTest)
	fmt.Printf("message from the command %s \n",  c.getMyName())
}


type CommandTest struct {
}

func (c *CommandTest) getMyName() string {
	return "my name is CommandTest"
}

func TestNewMemoryCommandBus(t *testing.T) {

	var expectedInterface *CommandBusInterface
	b := NewMemoryCommandBus()
	assert := assert.New(t)
	assert.Implements(expectedInterface,b)


}

func TestMemoryCommandBus_Register(t *testing.T) {

	b := NewMemoryCommandBus()
	b.Register(new(mockCommandHandler), new(mockCommandHandler))
}

func TestMemoryCommandBus_Send(t *testing.T) {
	b := NewMemoryCommandBus()
	b.Register(CommandTest{}, new(mockCommandHandler))
	c := CommandTest{}
	b.Send(c)
}