package Validator

import "regexp"

const InvalidEmail  = "invalid:email"

func Email(s string) bool {
	return regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`).MatchString(s)
}
