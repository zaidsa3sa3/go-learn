package Validator

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestEmail(t *testing.T) {

	assert := assert.New(t)

	assert.Equal(true,Email("test@gmail.ci"))
	assert.Equal(true,Email("tt@g.com"))
	assert.Equal(true,Email("t@gmail.comm"))

	assert.Equal(false,Email("test@gmail.c"))
	assert.Equal(false,Email("test@gmail.cooom"))
	assert.Equal(false,Email("@gmail.com"))
	assert.Equal(false,Email("gmail.com"))
	assert.Equal(false,Email(".com"))
	assert.Equal(false,Email("com"))
}
