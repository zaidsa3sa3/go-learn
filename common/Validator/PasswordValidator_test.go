package Validator

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestPassword(t *testing.T) {

	assert := assert.New(t)

	assert.Equal(true,Password("123456"))
	assert.Equal(true,Password("qazwsxedcc"))
	assert.Equal(true,Password("qaz@#$wsx"))


	assert.Equal(false,Password("12345"))
	assert.Equal(false,Password("passw"))
	assert.Equal(false,Password("pass"))
}
