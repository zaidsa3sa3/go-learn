package config

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"log"
	"bitbucket.org/zaidsa3sa3/go-learn/config/static"
)

type config struct {
	Application *static.Application
	RPC *rpc
}

var(
	Configuration *config
)

const (
	rpcNamespace = "%s_RPC"
)

func init()  {

	a := &static.Application{
		Name: "ACCOUNT_SERVICE",
		Version: "DEVEL",
	}

	var err error
	Configuration , err= createConfiguration(a)
	if err != nil {
		log.Fatalf("failed to create configuration: %v", err)
	}
}


func createConfiguration(a *static.Application) (*config, error) {

	var g rpc
	err := envconfig.Process(fmt.Sprintf(rpcNamespace,a.Name), &g)

	return &config{
		Application: a,
		RPC: &g,
	}, err
}
