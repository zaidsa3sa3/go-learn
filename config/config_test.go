package config

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/zaidsa3sa3/go-learn/config/static"
	"os"
	"fmt"
	"strconv"
)

func TestConfig_CreateConfigurations(t *testing.T)  {

	assert := assert.New(t)

	a := &static.Application{
		Name: "TEST_APP",
		Version: "devel",
	}

	expectedRPCProtocol := "test"
	expectedRPCPort := 1234

	// Set DB environments
	os.Setenv(fmt.Sprintf(rpcNamespace + "_PROTOCOL", a.Name), expectedRPCProtocol)
	os.Setenv(fmt.Sprintf(rpcNamespace + "_PORT", a.Name),  strconv.Itoa(expectedRPCPort))

	config, err := createConfiguration(a)

	assert.NoError(err)
	assert.Equal(expectedRPCProtocol, config.RPC.Protocol)
	assert.Equal(expectedRPCPort, config.RPC.Port)
	assert.Equal(a.Name, config.Application.Name)
	assert.Equal(a.Version, config.Application.Version)

}
