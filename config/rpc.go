package config

type rpc struct {
	Protocol 	string	`default:"tcp"`
	Port		int	`default:"5000"`
}

