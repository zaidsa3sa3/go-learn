#!/bin/bash
# Install a custom Go version, https://golang.org/
#
# Add at least the following environment variables to your project configuration
# (otherwise the defaults below will be used).
# * GO_VERSION
#
# Include in your builds via
# source /dev/stdin <<< "$(curl -sSL https://raw.githubusercontent.com/codeship/scripts/master/languages/go.sh)"
GO_VERSION=${GO_VERSION:="1.6"}
PROTO_VERSION=${PROTO_VERSION:="3.0.0"}

# strip all components from PATH which point toa GO installation and configure the
# download location
CLEANED_PATH=$(echo $PATH | sed -r 's|/(usr/local\|tmp)/go(/([0-9]\.)+[0-9])?/bin:||g')
GO_DOWNLOAD="${HOME}/go${GO_VERSION}.linux-amd64.tar.gz"
PROTO_DOWNLOAD="${HOME}/protoc-${PROTO_VERSION}-linux-x86_64.zip"
# configure the new GOROOT and PATH
export GOROOT="/usr/local/go/${GO_VERSION}"
#export PATH="${GOROOT}/bin:${CLEANED_PATH}"
export GOPATH="${HOME}/Projects/go"
export PATH="${PATH}:/usr/local/go/${GO_VERSION}/bin:${GOPATH}/bin:${HOME}/go/bin"
# no set -e because this file is sourced and with the option set a failing command
mkdir -p "${GOROOT}"
mkdir -p "${GOPATH}/bin"
wget --continue --output-document "${GO_DOWNLOAD}" "https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz"
tar -xaf "${GO_DOWNLOAD}" --strip-components=1 --directory "${GOROOT}"
# check the correct version is used
go get -t -v ./...


wget --continue --output-document "${PROTO_DOWNLOAD}" "https://github.com/google/protobuf/releases/download/v${PROTO_VERSION}/protoc-${PROTO_VERSION}-linux-x86_64.zip"

# run as . ./gosetup.sh