package main

import (
	"bitbucket.org/zaidsa3sa3/go-learn/proto"
	"net"
	"log"
	"google.golang.org/grpc"
	"bitbucket.org/zaidsa3sa3/go-learn/server"
	"fmt"
	"bitbucket.org/zaidsa3sa3/go-learn/config"
)

var (
	port = config.Configuration.RPC.Port
	protocol = config.Configuration.RPC.Protocol
	serviceName = config.Configuration.Application.Name
)

func main() {


	lis, err := net.Listen(protocol, fmt.Sprintf(":%d",port))

	fmt.Printf(fmt.Sprintf("%s started on port: %d",serviceName, port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	proto_account.RegisterAccountServer(s, &server.Account{})
	s.Serve(lis)
}

