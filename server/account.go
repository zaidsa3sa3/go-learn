package server

import (
	"golang.org/x/net/context"
	"bitbucket.org/zaidsa3sa3/go-learn/proto"
	"bitbucket.org/zaidsa3sa3/go-learn/command"
	"errors"
	"bitbucket.org/zaidsa3sa3/go-learn/common"
	"bitbucket.org/zaidsa3sa3/go-learn/commandHandler"
)

var (
	commandBus common.CommandBusInterface
)

func init()  {
	// Register Commands
	commandBus = common.NewMemoryCommandBus()
	commandBus.Register(command.RegisterAccount{}, new(commandHandler.RegisterAccount))
}

// server is used to implement AccountServer.
type Account struct{}

// CreateNewAccount implements AccountServer.CreateNewAccount
func (s *Account) CreateNewAccount(ctx context.Context, in *proto_account.CreateNewAccountRequest) (*proto_account.GetAccountResponse, error) {

	c := command.CreateRegisterAccount(in.Name, in.Email,in.Passowrd,in.ConfirmPassowrd)

	if c.IsValid() == false{
		return nil,errors.New("command not is valid")

	}

	commandBus.Send(*c)

	// TODO : implement DTO "ReadModel"

	return &proto_account.GetAccountResponse{Id:1,Name:in.Name, Email:in.Email}, nil

}


// GetAccountById implements AccountServer.GetAccountById
func (s *Account) GetAccountById(ctx context.Context, in *proto_account.GetAccountByIdRequest) (*proto_account.GetAccountResponse, error) {
	return &proto_account.GetAccountResponse{Id:in.Id,Name:"test", Email:"test@gmail.com"}, nil
}

// ChangeAccountPasswordRequest implements AccountServer.ChangeAccountPasswordRequest
func (s *Account) ChangeAccountPassword(ctx context.Context, in *proto_account.ChangeAccountPasswordRequest) (*proto_account.GetAccountResponse, error) {
	return &proto_account.GetAccountResponse{Id:in.Id,Name:"test", Email:"test@gmail.com"}, nil
}