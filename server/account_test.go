package server

import (
	"testing"
	"time"
	"bitbucket.org/zaidsa3sa3/go-learn/proto"
	"github.com/stretchr/testify/assert"
)

type mockContext struct {
}

func (*mockContext) Deadline() (deadline time.Time, ok bool)  {
	return time.Now(), false
}

func (*mockContext) Done() <-chan struct{} {
	return nil
}

func (*mockContext) Err() error {
	return nil
}

func (*mockContext) Value(key interface{}) interface{} {
	return nil
}

func TestAccount_CreateNewAccount(t *testing.T) {
	m := new(mockContext)
	req := new(proto_account.CreateNewAccountRequest)
	req.Name = "zaid"
	req.Email = "zaidsa3sa3@gmail.com"
	req.Passowrd = "password"
	a := new(Account)
	res, err:= a.CreateNewAccount(m,req)

	assert := assert.New(t);
	assert.Equal(int64(1),res.Id)
	assert.Equal(res.Name,res.Name)
	assert.Equal(res.Email,res.Email)
	assert.NoError(err)
}

func TestAccount_CreateNewAccount_WithError(t *testing.T) {
	m := new(mockContext)
	req := new(proto_account.CreateNewAccountRequest)
	req.Name = "zaid"
	req.Email = "zaidsa3sal.com"
	req.Passowrd = "password"
	a := new(Account)
	_, err:= a.CreateNewAccount(m,req)
	assert := assert.New(t);
	assert.Error(err)
}

func TestAccount_GetAccountById(t *testing.T) {
	m := new(mockContext)
	req := new(proto_account.GetAccountByIdRequest)
	req.Id = 1
	a := new(Account)
	res, err:= a.GetAccountById(m,req)

	assert := assert.New(t);
	assert.Equal(req.Id,res.Id)
	assert.Equal(res.Name,"test")
	assert.Equal(res.Email,"test@gmail.com")
	assert.NoError(err)
}

func TestAccount_ChangeAccountPassword(t *testing.T) {
	m := new(mockContext)
	req := new(proto_account.ChangeAccountPasswordRequest)
	req.Id = 1
	a := new(Account)
	res, err:= a.ChangeAccountPassword(m,req)

	assert := assert.New(t);
	assert.Equal(req.Id,res.Id)
	assert.Equal(res.Name,"test")
	assert.Equal(res.Email,"test@gmail.com")
	assert.NoError(err)
}